var m = require('mithril');

var Plugin = {};

Plugin.controller = function(props) {

};

Plugin.view = function(ctrl, props) {
  var style = {color: props.fontColor, backgroundColor: props.tileColor};

  return m('.Plugin', {style: style}, [
    m('h1', 'A Super Simple Crux Plugin')
  ]);
};

module.exports = Plugin;
