package sample //change this to your package name

import (
	"net/http"

	"git-lab.boldapps.net/proximity/crux/plugin"
	"github.com/labstack/echo"
)

func Plugin(dashboard *plugin.Crux) {

	// Do setup and initialization of resources here.

	// Need to expose some endpoints ?
	dashboard.Router.Get("/sample-plugin", func(c *echo.Context) error {
		data := map[string]interface{}{
			"samples": []string{"things", "are simpler", "in golang"},
		}
		return c.JSON(http.StatusOK, data)
	})
}
